#!/bin/python3
from errorcodes import puckrdy, err0004, err0010, err0020, err0021, err0022, err0023
from utils import printRed, printCyn, printYel, error, printGrn, warning
from genericProtocol import GenericProtocol
import os

allowedBaudRates = [9600, 19200, 38400, 57600, 115200]

#---- PUCK Protocol ----#
# This class implements the PUCK protocol commands.
class PUCKprotocol(GenericProtocol):
    def __init__(self, payloadFile, instrumentProtocol):
        GenericProtocol.__init__(self)        
        
        # The instrument is initialized in Instrument mode
        self.puckMode = False
        self.payloadFile = payloadFile
        self.size = os.path.getsize(payloadFile) # Get file size
        self.payload = bytearray(self.size) # declare an array of bytes        
        f = open(payloadFile, "rb")                    
        self.payload = f.read(self.size) # read file and store it at payload
        f.close()
        
        # store a pointer to the instrument protocol within PUCK, required to switch from
        # PUCK mode to Instrument mode
        self.instrumentProtocol = instrumentProtocol
        
        #--------------------------------------#
        #              PUCK Datasheet          #
        #----------------------------#---------#
        #  Description       # size  #  index  #
        #----------------------------#---------#     
        # UUID               #  16   #    0    #
        # Datasheet Version  #   2   #   16    #
        # Datasheet size     #   2   #   18    #
        # Manufacturer ID    #   4   #   20    #
        # Manufacturer Model #   2   #   24    #
        # Man. Version       #   2   #   26    #
        # Serial Number      #   4   #   28    #
        # Instrument Name    #  64   #   32    #
        #----------------------------#---------#
        
        # memoryview(buff, size, offset)
                
        self.datasheet = memoryview(self.payload[0:95])
        self.UUID = memoryview(self.datasheet[0:15]) 
        self.version = memoryview(self.payload[16:17])
        self.datasheetSize = memoryview(self.payload[18:19]) 
        self.manufacturererID = memoryview(self.payload[20:23]) 
        self.manufacturererModel = memoryview(self.payload[24:25]) 
        self.manufacturererVersion = memoryview(self.payload[26:27])
        self.serialNumber = memoryview(self.payload[28:29])
        self.name = memoryview(self.payload[30:31])                             
            
        
        # print("datasheet: \n", bytes(self.datasheet))        
        self.pointer = 0
        self.writeSession = False
        
        
        self.addCommand(b'PUCKRM',self.PUCKRM)
        self.addCommand(b'PUCKWM',self.PUCKWM)
        self.addCommand(b'PUCKEM\r',self.PUCKEM)
        self.addCommand(b'PUCKFM\r',self.PUCKFM)
        self.addCommand(b'PUCKGA\r',self.PUCKGA)
        self.addCommand(b'PUCKSA',self.PUCKSA)
        self.addCommand(b'PUCKSZ\r',self.PUCKSZ)
        self.addCommand(b'PUCKTY\r',self.PUCKTY)
        self.addCommand(b'PUCKVR\r',self.PUCKVR)
        self.addCommand(b'PUCK\r',self.PUCK)
        self.addCommand(b'PUCKIM\r',self.PUCKIM)
        self.addCommand(b'PUCKVB',self.PUCKVB)
        self.addCommand(b'PUCKSB',self.PUCKSB)

        
        #---- PUCKRM ----#
    def PUCKRM(self):        
        print('Handler for PUCK command PUCKRM')
        arg = self.readUntil(b'\r', 4)
        arg = arg.strip()
                        
        if arg.isdigit() == False: 
            printRed('Argument not a number')
            return err0004
                
        nbytes = int(arg)
        
        print(nbytes, "bytes requested")
        # checking argument
        if nbytes > self.size or nbytes < 0 :
            printRed("Requested number out of range")
            return err0020
        
        
        if nbytes + self.pointer < self.size :
            chunk = self.payload[self.pointer:self.pointer + nbytes]
        else : # Rollover
            chunk = self.payload[self.pointer:] # from the pointer to the end
            bytesleft = nbytes + self.pointer - self.size
            chunk += self.payload[0:bytesleft]
            self.pointer = bytesleft
            printYel("Rollover, new address", self.pointer)
                        
                                     
        # get the bytes                    
        response = b'[' + chunk + b']' + puckrdy
        
        # rollover condition
        if (self.pointer + nbytes) > self.size :
            self.pointer = 0
            print("Reached end of payload, rolling over to 0")
        else : 
            self.pointer += nbytes
            
        return response
    

    #---- PUCKWM ----#
    def PUCKWM(self) :
        print('Handler for PUCK command PUCKWM')
                
        arg = self.readUntil(b'\r', 4)
        arg = arg.strip()
        
        if arg.isdigit() == False: 
            printRed('Argument not a number')
            return err0004        
        nbytes = int(arg)
        
        print("Expecting", nbytes, "bytes")
        if self.writeSession == False : 
            error("Memory not initialized")
            return err0023 + puckrdy
                                   
        
        # check that nbytes is between 0 and 32
        if nbytes < 0 or nbytes > 32:
            printRed("Trying to write an invalid number of bytes")
            return err0004
                
        if self.pointer + nbytes > self.size :
            error("Trying to write outside puck memory")
            return err0021 + puckrdy 
        
        
        # Read nbytes from serial port
        try :
            chunk = self.getBytes(nbytes, 10)
        except Exception as e:
            error("Exception", e)
            return err0004
        
            
        print("Writing", nbytes, "bytes,", chunk)                
        
                
        if self.pointer == 0 :
            printGrn("Start")
            print(len(chunk))
            print(len(self.payload[nbytes:]))
            self.payload = chunk + self.payload[nbytes:]
        
        else :           
            self.payload = self.payload[0:self.pointer] + chunk + self.payload[self.pointer + nbytes:]
        
        self.pointer += nbytes
        print("PUCK memory has size", len(self.payload))
        
        
        # add handler code here #
        return puckrdy


    #---- PUCKFM ----#
    def PUCKFM(self):            

        if self.writeSession == False : 
            return err0023 + puckrdy
        
        f = open(self.payloadFile, "wb")
        f.write(self.payload)
        f.close()
        self.writeSession = False        
        return puckrdy


    #---- PUCKEM ----#
    def PUCKEM(self):
        print("Erasing all memory")
        # Clear the memory
        self.payload = '\0'.encode() * self.size        
        self.writeSession = True
        return puckrdy


    #---- PUCKGA ----#
    def PUCKGA(self):
        print('Handler for PUCK command PUCKGA')

        # add handler code here #
        response = str(self.pointer) + '\r'
        print("returning address", self.pointer)
        return response.encode() + puckrdy


    #---- PUCKSA ----#
    def PUCKSA(self):
        print('Handler for PUCK command PUCKSA')

                
        arg = self.readUntil(b'\r', 4)
        arg = arg.strip()
        
        if arg.isdigit() == False: 
            printRed('Argument not a number')
            return err0004        
        newPosition = int(arg)
        
        if newPosition < 0 or newPosition >= self.size :
            print ("new position out of bounds")
        
        print("setting pointer to", newPosition)
        self.pointer = newPosition
        return puckrdy


    #---- PUCKSZ ----#
    def PUCKSZ(self):
        print('Handler for PUCK command PUCKSZ')        
        return str(self.size).encode() + '\r'.encode() + puckrdy


    #---- PUCKTY ----#
    def PUCKTY(self):
        print('Handler for PUCK command PUCKTY')
        return '0000\r'.encode() + puckrdy


    #---- PUCKVR ----#
    def PUCKVR(self):
        print('Handler for PUCK command PUCKVR')        
        return 'v1.4\r'.encode() + puckrdy


    #---- PUCK ----#
    def PUCK(self):
        print('Handler for PUCK command PUCK')
        return puckrdy


    #---- PUCKIM ----#
    def PUCKIM(self):
        print('Handler for PUCK command PUCKIM')
        self.puckMode = False
        self.instrumentProtocol.initProtocol()
        return b''


    #---- PUCKVB ----#
    def PUCKVB(self):
        print('Handler for PUCK command PUCKVB')
        arg = self.readUntil(b'\r', 4)
        arg = arg.strip()
        
        if arg.isdigit() == False: 
            printRed('Argument not a number')
            return err0004 + puckrdy        
        baudrate = int(arg)
        
        if baudrate in allowedBaudRates :
            reply = b'YES\r'
        else :
            reply = b'NO\r'
            
        return reply + puckrdy

    #---- PUCKSB ----#
    def PUCKSB(self):
        print('Handler for PUCK command PUCKSB')
        arg = self.readUntil(b'\r', 4)
        arg = arg.strip()
        
        if arg.isdigit() == False: 
            printRed('Argument not a number')
            return err0004        
        baudrate = int(arg)
        print("Setting baudrate to", baudrate)
        self.serial.baudrate = baudrate
        return puckrdy
    
    