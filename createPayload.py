#!/usr/bin/python3

import sys
from utils import error, warning, printCyn
import hashlib
import os



# Gets the option from a buffer, first gets the line and then extracts the value
# btween ""
def getOption(buffer, option):
    
    list = [line for line in buffer.split('\n') if option in line]
    if len(list) < 1 :
        error("string not found", option)
        return None
    line = list[0]    
    splitted = line.split("\"")
    result = splitted[1]    
    if len(result) < 1 :
        error("Invalid line:", line) 
        return result    
    return result


def stringToBin(s, length):
    
    if len(s) == 1 :
        s = '0' + s
        
    bin = bytearray.fromhex(s)
    
    if (len(bin) < length) :
        # warning("Length does not match!, appending 0 at the beginning")
        zeros = bytearray(length-len(bin))
        bin = zeros + bin
    elif len(bin) > length :
        error("BIN is bigger than expected", len(bin), length)
    
    return bin


class Datasheet():
    def __init__(self, input_file):
       
        print("Opening datasheet options file", input_file) 
        f = open(input_file, "r")
        buff = f.read()
        f.close()
        

        # Get the Instrument UUID
        tmp = getOption(buff, "Instrument UUID")
        tmp = tmp.replace('-', '')        
        self.uuid = stringToBin(tmp, 16)
        print("UUID", self.uuid)
        self. datasheetVersion = stringToBin(getOption(buff, "Datasheet Version"), 2)
        print("Datasheet Version", self.datasheetVersion)
        
        self. datasheetSize = stringToBin(getOption(buff, "Datasheet Size"), 2)
        print("Datasheet Size", self.datasheetSize)
        
        self. manufactureID = stringToBin(getOption(buff, "Manufacture ID"), 4)
        print("Manufacture ID", self.manufactureID)
        
        self. manufactureModel = stringToBin(getOption(buff, "Manufacture Model"), 2)
        print("manufactureModel", self.manufactureModel)
        
        self. manufactureVersion = stringToBin(getOption(buff, "Manufacture Version"), 2)        
        print("Manufacture Version", self.manufactureVersion)
        
        self. serialNumber = stringToBin(getOption(buff, "Serial Number"), 4)                    
        print("Serial Number", self.serialNumber)
        
        tmpname = getOption(buff, "Instrument Name")
         
        if len(tmpname) < 64 :
            self.instrumentName = tmpname.encode() + bytearray(64-len(tmpname))
        else :
            self.instrumentName = tmpname.encode()
         
        
        if len(self.instrumentName) > 64 :
            error("Instrument Name has max length of 64 bytes, now has", len(self.instrumentName), "bytes")
            return None
        print("Instrument Name", self.instrumentName)
        
        
        self.datasheet = self.uuid + self.datasheetVersion + \
            self.datasheetSize + self.manufactureID + self.manufactureModel + \
            self.manufactureVersion + self.serialNumber + self.instrumentName  
 





class Payload():
    def __init__(self,datasheetOptions, size):
        self.datasheet = Datasheet(datasheetOptions)

        self.tag = ''
        self.size = int(size)
         
    
    def createTag(self, fileToStore, filetype="SWE-SensorML"):
        print("Creating Tag for file", fileToStore, "...")        
        fileSize = os.path.getsize(fileToStore)
        fileName = os.path.basename(fileToStore)        
        next_addr = -1
        version = 1
        

        f = open(fileToStore, "rb")
        buff = f.read()
        f.close()
        
        checksum = hashlib.md5(buff).hexdigest()
        self.tag = "<puck_payload type=\"" + filetype + "\" name=\"" + os.path.basename(fileToStore) + "\" size=\"" + \
            str(fileSize) + "\" next_addr=-1\" " + "version=\"1\"/>"
        
        printCyn(self.tag)
        
        self.fileToStore = fileToStore
    
    def create(self, outputFile, inputFile):
            
            infile = open(inputFile, "rb")
            buff = infile.read()
            infile.close()            
            self.createTag(inputFile)            
            f = open(outputFile, "wb")
            nbytes = f.write(self.datasheet.datasheet)
            nbytes += f.write(self.tag.encode())
            nbytes += f.write(buff)
            print("Written ", nbytes, "bytes")
            zeros = bytearray(self.size - nbytes)
            print("Filling end of payload with", len(zeros), "zeros")                                    
            f.write(zeros)                    
            f.close()    
            print("PUCK Payload file", outputFile, "  created, with ", nbytes + len(zeros), "bytes")

        
 
#----------------------------------------------------------------#
#------------------------ PROGRAM STARTS HERE -------------------#
#----------------------------------------------------------------#               
if len(sys.argv) != 5 :
    
        #error("Options file expected")
    print("usage:", sys.argv[0],"<file to store> <payload size> <datasheet file> <payload filename>\n")
    print("    File in Payload: File that will be embedded within the PUCK Payload memory")
    print("    Payload Size: Size of the newly created payload")
    print("    Datasheet File: Datasheet options file")
    print("    Datasheet File: Output payload filename")
    exit()
    


fileToStore = sys.argv[1]
payloadSize = sys.argv[2]
datasheetOptions = sys.argv[3]
payloadFilename = sys.argv[4]
p = Payload(datasheetOptions, payloadSize)
p.create(payloadFilename, fileToStore)









