# success
puckrdy = 'PUCKRDY\r'.encode()

# Error 0004, command not found 
err0004 = 'ERR 0004\r'.encode() 

# Error 0010, invalid baud rate requested 
err0010 = 'ERR 0010\r'.encode()

# Error 0020 Bytes requested for read or write out of range
err0020 = 'ERR 0020\r'.encode()

# Error 0021 Address requested out of range
err0021 = 'ERR 0021\r'.encode()

# Error 0022 Write attempt for read only memory
err0022 = 'ERR 0022\r'.encode()

# Error 0023 Write attempt to non-initialized memory
err0023 = 'ERR 0023\r'.encode()



