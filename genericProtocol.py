from random import uniform, randint
from utils import warning, printBlu, printYel
from myTimer import myTimer
import time
import sys



# This class implements generic functions that can be used in any
# protocol implemented
class GenericProtocol():
    def __init__(self):           
        self.commands = [] # declare the patterns that match the commands as an empty list
        self.handlers = []        
        
    def addCommand(self, command, handler):
        self.commands.append(command)
        self.handlers.append(handler)
    
    def assignSerial(self, serial):
        self.serial = serial

    # Waits until N bytes are received in the serial port
    def getBytes(self, n, timeout = 10, exception = False):            
        timer = myTimer(timeout, exception)
        buff = b'' # declare a binary buffer
        #while timer.timeout == False or token.encode() in buff :
        while timer.timeout == False : #or token.encode() in buff :
            while self.serial.inWaiting() > 0 :
                
                c = self.serial.read(1)
                buff += c
                sys.stdout.write(str(c))
                              
                if len(buff) == n :                 
                    break    
                                                                                                                        
            if len(buff) == n :                 
                    break
            time.sleep(0.1) # sleep for 10ms
            
                      
        if timer.timeout == True : 
            warning("Timeout")   
        return buff
            
    
    def readUntil(self, token, timeout = 10, exception = False):
        print("Reading until ", token, "is found")
        timer = myTimer(timeout, exception)
        buff = b'' # declare a binary buffer
        #while timer.timeout == False or token.encode() in buff :
        while timer.timeout == False : #or token.encode() in buff :
            while self.serial.inWaiting() > 0 :

                if self.serial.inWaiting() > 0 : #and not token.encode() in buff:
                    c = self.serial.read(1)
                    buff += c
                    sys.stdout.write(str(c))
                
                if token in buff : 
                    break
            if token in buff : 
                break
                                                                                                    
            time.sleep(0.1) # sleep for 10ms            
        if timer.timeout == True : 
            warning("Timeout")   
        return buff
    
    

    
    
        # Generate a floating point value
    def generateFloat(self,precission = 4, lower = 0, upper = 100):
        return str(float(round(uniform(lower, upper), precission)))
    
    
    # generate n floating point values separated by a token
    def generateFields(self,nfields, tokenSeparator, precission = 4, lower = 0, upper = 100):
        resp = self.generateFloat(precission, lower, upper) 
        for i in range(nfields -1) :
            resp += tokenSeparator + self.generateFloat(precission, lower, upper)
        return resp
    
    
    # Init protocol is called every time the sensor enters in Instrument Mode
    def initProtocol(self):
        print("Initializing genericProtocol")

        pass
    
    # exitInstrumentMode is called every time the sensor switches to PUCK Mode
    def exitInstrumentMode(self):
        print("Exit Instrument Mode (generic)    ")
        pass

    # Stream is supposed to be a binary array
    def randomizeResponse(self, stream):
        length = len(stream)
        randomizedElements = randint(1,int(length/5))
        
        print(stream)
        while randomizedElements > 0 :
            randomizedElements -= 1
            newElement = randint(0,255)
            position = randint(0,length) 
            stream = stream[0:position] + bytes([newElement]) + stream[position + 1:]
        
                
        return stream

        
        
        
        
        