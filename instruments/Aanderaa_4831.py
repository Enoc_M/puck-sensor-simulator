#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

from time import strftime
from genericProtocol import GenericProtocol
from threading import Timer
from random import random
from utils import printYel, printBlu

period = 1
wrongResponses = 0.4 # 10% of responses are going to be wrong
    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)

        
    def exitInstrumentMode(self):
        self.t.cancel()
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing Aanderaa 4831")                                
        self.t = Timer(period, self.stream)
        self.t.start()
        
    
    def stream(self):
        reply = '4381\t606\t'
        reply += self.generateFields(10, '\t', 3) + '\r\n'
        
        
        encodedReply = reply.encode()
        
        if random() < wrongResponses :
            print("Randomizing response")
            encodedReply= self.randomizeResponse(encodedReply)
            printYel(encodedReply)
        else :
            printBlu(reply)
        self.serial.write(encodedReply)
        self.t = Timer(period, self.stream)
        self.t.start()
        return None
    
    
    
    
    
        