#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

import time
from genericProtocol import GenericProtocol
from threading import Timer
from random import uniform



# duration is in seconds
period = 2
    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)

        
    def exitInstrumentMode(self):
        self.t.cancel()
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing ECO NTU")
        self.t = Timer(period, self.stream)
        self.t.start()
        
    
    def stream(self):
        reply =  '99/99/99\t99:99:99:\t' + "%d" % uniform(100,999) + '\t' "%d" % uniform(100,999)+'\t' +"%d" % uniform(100,999) +'\r\n'
        print(reply)
        self.serial.write(reply.encode())
        self.t = Timer(period, self.stream)
        self.t.start()
        return None
         
      
    
     
        
    