#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

import time
from genericProtocol import GenericProtocol
from threading import Timer
from random import uniform

period = 5

    # duration is in seconds
    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)
        self.addCommand(b'ts', self.takeSample)
        self.addCommand(b'START', self.startStream)
        self.addCommand(b'STOP', self.stopStream)
        
        self.streaming = False

        
    def exitInstrumentMode(self):
        self.stopStream()
        self.streaming = False
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing SBE37")
        
    
    def stopStream(self):
        if self.streaming == True :
            self.t.cancel()
        
        
    
    def startStream(self):
        self.t = Timer(period, self.stream)        
        self.t.start()
        self.streaming = True
        return None
    
    def takeSample(self):
        reply = self.generateFields(5, ',', 5) + time.strftime("%d %b %Y, %H:%m:%S\r\n")
        return reply.encode()
    
    def stream(self):
        reply = self.takeSample()
        print(reply)
        self.serial.write(reply)
        self.t = Timer(period, self.stream)
        self.t.start()
        return None