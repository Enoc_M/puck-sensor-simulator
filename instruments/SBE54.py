#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

from time import strftime
from genericProtocol import GenericProtocol
from threading import Timer


    # duration is in seconds

period = 1    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)

        
    def exitInstrumentMode(self):
        self.t.cancel()
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing SBE 54")                                
        self.t = Timer(period, self.stream)
        self.t.start()
        
    
    def stream(self):
        reply = '''<Sample Num='684549' Type='Pressure'>
<Time>2016-11-15T00:04:10</Time>
<PressurePSI>''' + self.generateFloat(4, 0, 100) + '''</PressurePSI>
<PTemp>''' + self.generateFloat(4, 0, 100) + '''</PTemp>
</Sample>''' + '\r\n'
                
        self.serial.write(reply.encode())
        self.t = Timer(period, self.stream)
        self.t.start()
        return None