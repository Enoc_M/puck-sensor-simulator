#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

from time import strftime
from genericProtocol import GenericProtocol
from threading import Timer
from random import random
from utils import printYel, printBlu

period = 2
wrongResponses = 0.1 # 10% of responses are going to be wrong
    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)
        self.addCommand(b'0R!', self.takeSample)
        self.count = 0

        
    def exitInstrumentMode(self):
        self.t.cancel()
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing Cyclops-6k Turbidity")                                
        #self.t = Timer(period, self.stream)
        #self.t.start()
        

        
    def takeSample(self):
        reply = str(self.count) + '+324+0.1+23.6+52.3+1013.1+0.00+0+0.0+0.0+12.1\r\n'
        return reply.encode()
    
    
    
    
    
        