#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

from time import strftime
from genericProtocol import GenericProtocol
from threading import Timer


period = 60
    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)

        
    def exitInstrumentMode(self):
        self.t.cancel()
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing Workhorse ADCP")                                
        self.t = Timer(period, self.stream)
        self.t.start()
        
    
    def stream(self):
        bins = 20
        header = strftime("%Y-%m-%d,%H:%M:%S") +  self.generateFields(4, ',', 2)        
        reply = header
        for i in range(bins) :
            reply += ',\r\n' + str(i + 1 ) + ',' + self.generateFields(4, ',', 3) 
        reply += '\r\n'
                
        self.serial.write(reply.encode())
        self.t = Timer(period, self.stream)
        self.t.start()
        return None
         
      
    
     
        
    