#!/usr/bin/python3

# This file is an example of the protocol file that needs to be implemented for each instrument.
# At least the following 

# Protocol Class defines the protocol for the instrument
# that is being simuated

import time
from genericProtocol import GenericProtocol
from utils import error, printCyn

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)
                            
        # Add all the commands to the 
        self.addCommand(b'getSample?\r',self.exampleSample)
        self.addCommand(b'getEpoch?\r',self.exampleDate)
        self.addCommand(b'getValue?\r',self.getValue)
        self.addCommand(b'setValue',self.setValue)

    
    # This example method shows how a command should work
    # We expect the command "getSample?\r" and we reply "<value1>,<value2>,<value3>\r"    
    def exampleSample(self):
        reply = self.generateFields(3, ',') + '\r\n'
        return reply.encode() 
    
    def exampleDate(self):
        epoch_time = int(time.time())
        reply = str(epoch_time).encode() + '\r\n'.encode() 
        return reply
    
    def setValue(self):
        buff = self.readUntil(b'\r')
        buff = buff.strip() # delete spaces, CR and LF
        if buff.isdigit() != True :
            error("setValue argument MUST be a number, received", buff)
            return "error".encode()
        
        self.value = int(buff)
        print("Setting value to", self.value)
        
        return "OK\r\n".encode()
        
    def getValue(self):
        printCyn("Executing method \"getValue\"")
        reply = str(self.value) + '\r\n'
        return reply.encode()
        

    
                            
 
