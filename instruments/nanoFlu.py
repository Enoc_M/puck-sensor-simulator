
from time import strftime
from genericProtocol import GenericProtocol
from threading import Timer
import random
from utils import printYel, printBlu

period = 2
wrongResponses = 0.1 # 10% of responses are going to be wrong
    

class Protocol(GenericProtocol):
    def __init__(self):
        GenericProtocol.__init__(self)
        self.addCommand(b'MEAS\n', self.takeSample)
        self.count = 0

        
    def exitInstrumentMode(self):
        #self.t.cancel()
        print("Exit instrument mode")
        
    def initProtocol(self):
        print("Initializing nanoFlu")                                
        #self.t = Timer(period, self.stream)
        #self.t.start()
        

        
    def takeSample(self):
        reply = ' ' + str(round(random.uniform(1,100), 3))
        reply += '         ' + str(round(random.uniform(1,100), 2)) + '\r\n'
        return reply.encode()
    
