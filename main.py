#!/usr/bin/env python3

from serial import Serial
import time
import sys
import os
from PUCK import PUCKprotocol
from utils import *
import imp
from myTimer import myTimer



defaultBaudrate = 9600


# Sensor Class
# This class contains both protocols (propritary and PUCK), as well
# as the serial port management and the parser.
class Sensor():
    def __init__(self, protocolFile, puckPayloadFile):
        print("Initializing Sensor...")
        print("Importing sensor protocol from file", protocolFile)
        # Check if file exists
        if not os.path.isfile(protocolFile) :
            error("File does not exist:", protocolFile)
            exit()        
        
        # Initialize the Sensor Protocol
        print("Initializing sensor protocol...")
        protocolModule = imp.load_source("Protocol", protocolFile)         
        self.protocol = protocolModule.Protocol()
        self.protocol.initProtocol()
        
        # Initialize PUCK Protocol
        print("Initializing PUCK protocol...")
        self.puck = PUCKprotocol(puckPayloadFile, self.protocol)
        
        # declare an empty binary buffer
        self.buff = b'' 
        # PUCK mode flag        
        
        printGrn("Sensor initialized")
        
    # This function opens a serial port
    def openSerial(self, device, baudrate):                
        print("Opening serial port", device, "with baudrate", baudrate, "...")
        self.serial = Serial()                                 
        self.serial.port = device
        self.serial.baudrate = baudrate
        
        try : 
            self.serial.open()            
        except Exception as e :
            print("Could not open port")
            error(e)
            print("The application may misbehave ")
            # exit()
        
        if not self.serial.isOpen() :
            error("Could not open Serial port, exit")
            exit()
        
        self.protocol.assignSerial(self.serial)
        self.puck.assignSerial(self.serial)
        
    
    def run(self):
        while(1) :
            # if buffer is empty set a 30s timeout
            if len(self.buff) == 0 :
                timeout = myTimer(30, True)
            
            try :                 
                while self.serial.inWaiting() > 0 :
                    timeout.reset()                                                                        
                    self.charIn(self.serial.read(1))
             
                    time.sleep(0.01) # sleep for 10ms
            except Exception as e : 
                error("Exception:", e)
                print("Clearing buffer")
                self.buffer = b'' # empty the buffer
    
                
    def charIn(self, input):
        printChar(str(input))
        self.buff += input          
                
        # Step 1: Check for a SOFTBREAK        
        if self.puck.puckMode == False :  
            self.softbreakDetector()
        
        if self.puck.puckMode == False : 
            for cmd in self.protocol.commands :
                if cmd in self.buff : 
                    print("Found command", cmd, "in buffer", self.buff)
                    # empty buffer
                    self.buff = b''
                    i = self.protocol.commands.index(cmd)                
                    print("Executing handler...")
                    reply  = self.protocol.handlers[i]()
                    type(reply)
                    if reply is not None :                                        
                        if len(reply) > 0 : 
                            print("Returned", reply)
                            self.serial.write(reply)
                            
                            self.buff = b'' # empty the buffer


        else  : # PUCK Mode 
            for cmd in self.puck.commands :                
                if cmd in self.buff : 
                    print("Found command", cmd, "in buffer", self.buff)
                    # empty buffer
                    self.buff = b''
                    i = self.puck.commands.index(cmd)                
                    print("Executing handler...")
                    self.buff = b'' # empty the buffer
                    reply  = self.puck.handlers[i]()
                    if len(reply) > 0 : 
                        print("Returned", reply)
                        self.serial.write(reply)                    
                    self.buff = b''
                    

               
        
        
                
    # This function detects a softbreak
    def softbreakDetector(self):        
        # If we are in puck mode ignore any softbreak
        if self.puck.puckMode == True :
            return None
        
        if b'@@@@@@!!!!!!' in self.buff :            
            
            self.protocol.exitInstrumentMode()
            printGrn("Entering in PUCK Mode")
            self.puck.puckMode = True
            self.buff = b''
            self.serial.write(b'PUCKRDY\r')
                           
                                            
    
    def command(self, inCommand):
        # Process command
        cmd = self.parser(inCommand)
        response = self.puckCommand(cmd)
        # sendtoserial response
        return response     
                       
    def parser(self, buff):
        
        parts = buff.split('\r', 1) # delete last \r
        cmd = parts[0].split(' ', 1)   # split command and argument 
        if "PUCKWM" in buff :
            cmd.append(parts[1])                                                        
        return cmd
    







#-------------------------------------------------------------------#
#-------------------------- START PROGRAM --------------------------#
#-------------------------------------------------------------------#
if __name__ == "__main__" :
    
    if len(sys.argv) != 5 : 
        error("Invalid arguments")
        printCyn("usage", sys.argv[0], "<serial device> <baudrate> <puck payload> <protocol file>")
        print("Example", sys.argv[0], "/dev/ttyUSB0 9600 payload.puck instruments/ECO_NTU.py")
        exit()
    
    
    # get the device
    device = sys.argv[1]
    print("device:", device)
    
    # get the baudrate
    if sys.argv[2].isdigit() == False :
        error("Argument baudrate must be a number, got", sys.argv[2])
        print("Setting default baudrate", defaultBaudrate)
        baudrate = defaultBaudrate
    else :
        baudrate = int(sys.argv[2])        
        print("baudrate:", baudrate)
    
    # get the puck payload file
    payloadFile = sys.argv[3]
    if not os.path.isfile(payloadFile) :
        error("PUCK Payload is not a file:", payloadFile)
        exit()
    
        
    # get the puck payload file
    payloadFile = sys.argv[3]
    if not os.path.isfile(payloadFile) :
        error("PUCK Payload is not a file:", payloadFile)
        exit()

    # get the protocol file
    protocolFile = sys.argv[4]
    if not os.path.isfile(protocolFile) :
        error("Protocol File is not a file:", protocolFile)
        exit() 
        
    printBlu("Intializing PUCK Sensor Simulator")
    # Initialize the sensor
    sensor = Sensor(protocolFile, payloadFile )
    # Open the port
    sensor.openSerial(device, baudrate)
    # Start running
    print("Running")
    
    try :
        sensor.run()
    
    except Exception as e:        
        error(e)
        print("Closing serial port")
        sensor.serial.close()
        print("Exit")
        exit()
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

