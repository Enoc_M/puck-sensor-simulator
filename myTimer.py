#!/usr/bin/python3

import signal

# This class generates a timer for timeout based on UNIX signals
class myTimer():
    def __init__(self,secs,exception = False, loop = False):       
        self.secs = secs 
        self.timeout = False
        self.loop = loop
        self.exception = exception
        self.start()
        pass
    
    def cancel(self):
        signal.alarm(0)

        pass
    
    def reset(self):
        # resets the timeout
        self.cancel()
        self.start()
    
    def start(self):
        signal.signal(signal.SIGALRM, self.handler)
        signal.alarm(self.secs)

        
    
    def handler(self, signum, frame):
        self.timeout = True        
        if self.exception : 
            raise Exception("Timeout")
        


if __name__ == "__main__" :
    import time
    print("Example of timeout with exception")
    timer = myTimer(5, True)    
    try :
        while 1:
            print("sec")
            time.sleep(1)
    except Exception as exc : 
        print("Exception", exc)
    
    print("Example of timeout without exception")

    timer = myTimer(4)
    while timer.timeout == False :
        print("sec")
        time.sleep(1)
        
    print("Timeout expired")

# # Register an handler for the timeout
# def handler(signum, frame):
#     print("Forever is over!")
#     raise Exception("Timeout")
#     
# 
# # This function *may* run for an indetermined time...
# def loop_forever():
#     import time
#     while 1:
#         print("sec")
#         time.sleep(1)
#             
#             
# 
# 
# # Register the signal function handler
# signal.signal(signal.SIGALRM, handler)
# signal.alarm(10)
# 
# try:
#     loop_forever()    
# except Exception as exc: 
#     print(exc)
#     
# signal.alarm(0) # cancel the alarm 