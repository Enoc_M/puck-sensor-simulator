import sys

__all__ = ['printGrn', 'printBlu', 'printYel', 'printRed', 'printCyn', 'error', 'warning', 'printChar']

def getchar():
    sys.stdin.read(1)
     

__GRN = "\x1B[32m"
__BLU = "\x1B[34m"
__YEL = "\x1B[33m"
__RED = "\x1B[31m"
#MAG = "\x1B[35m"
__CYN = "\x1B[36m"
#WHT = "\x1B[37m"
#NRM = "\x1B[0m"
__RST = "\033[0m"   

def printChar(*arguments):
    sys.stdout.write(__CYN)
    sys.stdout.write(*arguments)
    sys.stdout.write(__RST)
    sys.stdout.flush()

def printGrn(*arguments):
    sys.stdout.write('\x1B[32m')
    print(*arguments)
    sys.stdout.write(__RST)
    
def printBlu(*arguments):
    sys.stdout.write(__BLU)
    print(*arguments)
    sys.stdout.write(__RST)
    
def printYel(*arguments):
    sys.stdout.write(__YEL)
    print(*arguments)
    sys.stdout.write(__RST)
    
def printRed(*arguments):
    sys.stdout.write(__RED)
    print(*arguments)
    sys.stdout.write(__RST)
    
def printCyn(*arguments):
    sys.stdout.write(__CYN)
    print(*arguments)
    sys.stdout.write(__RST)

def error(*arguments):
    sys.stdout.write(__RED)
    sys.stdout.write("ERROR: ")
    print(*arguments)
    sys.stdout.write(__RST)
    
def warning(*arguments):
    sys.stdout.write(__YEL)
    sys.stdout.write("WARNING: ")
    print(*arguments)
    sys.stdout.write(__RST)
    


